import java.io.*;


public class BufferedStreamExample {
    public static void main(String[] args) throws IOException {
        BufferedInputStream inStream = null;
        BufferedOutputStream outStream = null;
        try {
            inStream = new BufferedInputStream(new FileInputStream("/src/example.mp4"));
            outStream = new BufferedOutputStream(new FileOutputStream("/src/outStream.mp4"));
            // Byte Array buffer
            byte[] buffer = new byte[8298];    
            // Reads a byte at a time, if it reached the end of the file, returns -1 
            int content;
            //Reads bytes from the input stream into the buffer
            while ((content = inStream.read(buffer)) != -1) {
                //Writes the bytes from the buffer to the output stream starting from  index 0
                outStream.write(buffer, 0, content);
            }
            //Catches any exceptions that may occur during file input/output
        } catch (IOException e) {
            //prints the stack trace
            e.printStackTrace();
        }
    }
    
}