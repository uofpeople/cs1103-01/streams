import java.io.*;


class ByteStreamExample {
    public static void main(String[] args) throws IOException {
        FileInputStream inStream = null;
        FileOutputStream outStream = null;
        try {
            inStream = new FileInputStream("/src/inStream.jpg");
            outStream = new FileOutputStream("/src/outStream.jpg");
        // Reads a byte at a time, if it reached the end of the file, returns -1 
        int content;
        while ((content = inStream.read()) != -1) {
            outStream.write((byte) content);
        }

        } catch (IOException e) {
            e.printStackTrace();
        }
        // Close both streams
        finally {
            if (inStream != null) {
                inStream.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        }
    }
}