import java.io.*;

public class SerializeExample implements Serializable {
    public String product;
    public int productID;
    public static void main(String[] args){
        SerializeExample prod = new SerializeExample();
        prod.product = "Laptop";
        prod.productID = 1234842;
        try {
            FileOutputStream fileOut = new FileOutputStream("src/s.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(prod);
            out.close();
            fileOut.close();
            System.out.println("Object serialized and saved in s.txt");
        
            //Deserialization
            FileInputStream fileIn = new FileInputStream("/src/s.txt");

            ObjectInputStream in = new ObjectInputStream(fileIn);
            prod = (SerializeExample) in.readObject();
            in.close();
            fileIn.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("SerializeExample class not found");
            c.printStackTrace();
            return;
        }
        System.out.println("\nDeserialized SerializeExample");
        System.out.println("Product: " + prod.product);
        System.out.println("Product ID: " + prod.productID);
    }
}
